ChangeLog.md

License.md

Manifest.md

ReadMe.md

Security.md

_expand_lib_path.rb

bin/lorca

bin/lorca-alpine

lib/lorca/doc.rb

lib/lorca/manifest.rb

lib/lorca/expansions/cli.rb

lib/lorca/expansions/phrases/eff_large_wordlist.dir

lib/lorca/expansions/phrases/eff_large_wordlist.pag

lib/lorca/expansions/phrases.rb

lib/lorca/version.rb

lib/lorca.rb

test/_config/minitest.rb

test/bin/test_lorca.rb

test/lib/test_lorca.rb

test/lib/lorca/expansions/test_cli.rb

test/lib/lorca/expansions/test_phrases.rb

test/lib/lorca/test_doc.rb

test/lib/lorca/test_manifest.rb

test/lib/lorca/test_version.rb
