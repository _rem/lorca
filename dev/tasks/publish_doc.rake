# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-

namespace :doc do
  desc "Publish online documentation"
  task :publish => ["doc:build"] do
    FileUtils.mkdir "public"
    FileUtils.cp_r "./doc/.", "public"
  end
end
