# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-

desc "Show pending work"
task :wip do
  unless system "which", "ag", out: "/dev/null"
    puts  "ag not found"
    exit 1
  end

  tags = Regexp.union(%w[:NOTE: :FIXME: :TODO: :THINK: :WIP:]).to_s
  wip = system "ag", "--hidden", tags
  puts "Nothing to do" unless wip
end
