# Security

Security issues take precedence over bug fixes, and feature work.
Peer reviews, and security research, are also welcome to prevent users
from being compromised.


## Where should I report security issues?

Email directly `rem at disroot [dot] org` with details, and reproduction
steps. Please allow 90-days, from when we first reply to your report,
before public disclosure. At that time you may add a copy of your
published findings in the `disclosed` directory to help update users
about new, or improved security measures.

If you wish to be acknowledge below, mention it explicitly, and include

- Your name or alias
- Report date. YYYY-MM-DD
- Bug reported. Name or one-line description.
- Main contact (optional)

(Lack of a `disclosed` folder only means nothing has been reported)


## Configuration

According to [EFF's deep dive](https://www.eff.org/ar/deeplinks/2016/07/new-wordlists-random-passphrases)
into their wordlist released on 2016, each word has about 12.92 bits of
entropy.

By default, `Lorca` generates an 8-word passphrase (about 102.8 bits of
entropy). Which, considering the relevant calculations on this
[table](https://i.imgur.com/e3mGIFY.png), should take about 1.6 billion
years to guess when the cracker is able to generate 10^13 permutations
per second. Depending on the user's threat model `Lorca` may be configured
to return a passphrase with up to 53 words; about 684.76 bits of entropy.


## Checksum

Although it's possible to install gems with a trust policy, is not a
widely used feature. In practice, we'd have to allow installing gems
without a cert defeating the purpose of having a policy.

Instead, we include the checksum of the released gems, as recommended on
[rubygems](https://guides.rubygems.org/security/#include-checksum-of-released-gems-in-your-repository).
To verify the gem before installing it

```terminal
$ gem fetch lorca -v <version>
$ ruby -rdigest/sha2 -e "puts Digest::SHA512.new.hexdigest(File.read('lorca-<version>.gem'))"
```

Compare it with the hash in `checksum/lorca-<version>.gem.sha512` to
verify the integrity of the fetched gem. If the checksum matches

```terminal
$ gem install lorca -v <version>
```

If you wish to audit the gem locally before installation

```sh
$ gem unpack lorca -v <version>
```


## Acknowledgments

We would like to thank the following researchers:

| Name/alias | Date | Bug reported | Contact |
|------------|------|--------------|---------|
|            |      |              |         |
