# ChangeLog

## [Unreleased]

## [0.3.3]

### Fixed

- Path to dictionary file.
- Examples in the documentation.

### Added

- CI pipeline status to ReadMe.md

## [0.3.0]

### Security

- Use `Sysrandom` gem as pseudo-random generator for Ruby versions below 2.5.

### Added

- Lorca expansion system.
- LorcaCore expansion with base instance, and class, methods.
- Phrases expansion to generate passphrases. Based on EFF's diceware.
- CLI expansion to use Lorca from the terminal.

## [0.0.1]

### Security

- Added Manifest for everyone to keep track of gem files. Manually edited.

### Changed

- Extracted gemspec info to added modules.

### Added

- Lorca::LorcaDoc, and Lorca::LorcaManifest modules.
- License.
- ChangeLog for users to keep track of relevant gem changes.

## [0.0.0]

### Added

- Lorca gemspec with bare minimum of files, and settings.
