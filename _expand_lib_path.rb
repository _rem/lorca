# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-

lib = File.expand_path "./../lib", __FILE__
$LOAD_PATH.unshift lib unless $LOAD_PATH.include? lib
