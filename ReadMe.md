# Lorca

[![Version](https://badge.fury.io/rb/lorca.svg)](https://badge.fury.io/rb/lorca)
[![pipeline status](https://gitlab.com/_rem/lorca/badges/master/pipeline.svg)](https://gitlab.com/_rem/lorca/commits/master)


## Details

Lorca is a gem for generating passphrases based on the algorithm originally
described in [EFF Dice-Generated Passphrases](https://www.eff.org/dice).
It uses [EFF's long word list](https://www.eff.org/files/2016/07/18/eff_large_wordlist.txt).
The generator can be used from the command line interface, or added as
a library.

Passphrases, unlike passwords, are meant to be memorized. Check out these
links for a few ideas on how to memorize them:

- [Using memory effectively](https://www.studygs.net/memory/)
- [How to memorize lists](https://www.wikihow.com/Memorize-Lists)

Passphrases are great password manager keys.


## Quick Start

### Command line interface

Generate, and copy to clipboard, an 8-word passphrase.

```terminal
$ lorca -p
```

As Lorca is only capable of writing to the clipboard, it's up to the user
to paste it, and store it, somewhere safe.

For more details on how to use the CLI

```terminal
$ lorca
```

## System Requirements

- *nix OS.
- xsel or xclip (linux)
- Ruby 2.4+


## Installation

### CLI

To use Lorca as a command line interface:

```terminal
$ gem fetch lorca -v <version>
$ ruby -rdigest/sha2 -e "puts Digest::SHA512.new.hexdigest(File.read('lorca-<version>.gem'))"
```

Compare it with the hash in `checksum/lorca-<version>.gem.sha512` to
verify the integrity of the fetched gem. If the checksum matches

```terminal
$ gem install lorca -v <version>
```

### Gem

To use Lorca as a gem add it to the project's Gemfile:

```ruby
gem "lorca"
```

And then run

```bash
$ bundle install
```

For details on the API check out the [online documentation](https://_rem.gitlab.io/lorca).


### Updates

Subscribe to the [update notification feed](https://rubygems.org/gems/lorca/versions.atom).
Whenever there's a new release available read the
[ChangeLog.md](https://_rem.gitlab.io/lorca/files/ChangeLog_md.html)
for details on what changed since the last release.

#### CLI

Fetch and verify gem, as above,

```terminal
$ gem fetch lorca -v <version>
$ ruby -rdigest/sha2 -e "puts Digest::SHA512.new.hexdigest(File.read('lorca-<version>.gem'))"
```

then

```bash
$ gem update lorca
$ gem cleanup lorca
```

#### Gem

```bash
$ bundle update lorca
```

## License

Copyright (c) 2018-2019, René Maya. Licensed ISC. Read attached
`License.md` file for details.
