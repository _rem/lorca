# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-

require "minitest/autorun"
require "minitest/pride"
require "minitest/proveit"
require "minitest/unordered"
require_relative "./../../_expand_lib_path"

ENV["LORCA_ENV"] = "test"

class Minitest::Test
  make_my_diffs_pretty!
  prove_it!
end

module Minitest::Assertions
  def assert_frozen obj
    assert obj.frozen?, "Expected #{obj.inspect} to be frozen"
  end
end
