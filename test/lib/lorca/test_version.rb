# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-

require_relative "./../../_config/minitest"
require "lorca/version"

class TestLorcaVersion < Minitest::Test

  parallelize_me!

  def test_lorca_has_a_version
    assert_match %r{\d+.\d+.\d+}, Lorca::LorcaVersion
  end
end
