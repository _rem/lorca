# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-
require_relative "./../../../_config/minitest"
require "lorca"
require "lorca/expansions/phrases"

# Use stubs for built-in expansions. External ones should instanciate Lorca.
class TestLorcaPhrasesExpansion < Minitest::Test

  parallelize_me!

  def setup
    Lorca.add Lorca::Phrases
    @lorca = Lorca.new
  end

  def test_returns_a_phrase_with_a_pre_set_length
    @lorca.stub(:roll_word_id, "11111") do
      assert_match %r"\A(abacus *){8}\z", @lorca.phrase
    end
  end

  def test_writes_a_phrase_which_words_matches_the_given_number
    num = 10

    @lorca.stub(:roll_word_id, "11111") do
      assert_match %r/([a-z]+ *){#{num}}/, @lorca.phrase(words: num)
    end
  end

  def test_fails_to_return_a_phrase_given_less_words_than_default
    assert_raises(Lorca::LorcaCounterError) { @lorca.phrase words: 7 }
  end

  def test_fails_to_return_a_phrase_given_an_invalid_amount_of_words
    assert_raises(Lorca::LorcaCounterError) { @lorca.phrase words: Object.new }
  end

  def test_configures_min_words
    Lorca.add Lorca::Phrases, min_words: 9

    assert_equal 9, Lorca.settings[:phrases][:min_words]

    @lorca.stub(:roll_word_id, "11111") do
      assert_match %r"\A(abacus *){9}\z", @lorca.phrase
    end
  end

  def test_configures_max_words
    Lorca.add Lorca::Phrases, max_words: 12

    assert_equal 12, Lorca.settings[:phrases][:max_words]
    assert_raises { @lorca.phrase words: 13 }
  end

  def test_configures_dice_to_toss_per_word_id
    Lorca.add Lorca::Phrases, dice_to_toss: 3

    assert_equal 3, Lorca.settings[:phrases][:dice_to_toss]
    assert_raises { @lorca.phrase }
  end

  def test_configures_vocabulary
    path = "path/to/non/existent/vocab"
    Lorca.add Lorca::Phrases, vocabulary: path
    assert_equal path, Lorca.settings[:phrases][:vocabulary]
    assert_raises { @lorca.phrase }
  end
end
