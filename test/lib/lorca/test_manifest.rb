# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-
require_relative "./../../_config/minitest"
require "lorca/manifest"

class TestLorcaManifest < Minitest::Test

  parallelize_me!

  def test_has_gem_name
    assert_equal "lorca", Lorca::LorcaManifest.gem_name
  end

  def test_has_official_name
    assert_equal "Lorca", Lorca::LorcaManifest.official_name
  end

  def test_has_authors
    expected_authors = ["René Maya"]
    authors = Lorca::LorcaManifest.authors

    assert_equal expected_authors, authors
    assert_frozen authors
  end

  def test_has_contact_emails
    expected_emails = %w[rem@disroot.org]
    emails = Lorca::LorcaManifest.contact_emails
    assert_equal expected_emails, emails
    assert_frozen emails
  end

  def test_has_repo
    assert_equal "https://notabug.org/rem/lorca", Lorca::LorcaManifest.repo
  end

  def test_has_license
    assert_equal "ISC", Lorca::LorcaManifest.license
  end

  def test_has_metadata
    expected_metadata = {
      "source_code_uri"   => "https://notabug.org/rem/lorca",
      "bug_tracker_uri"   => "https://notabug.org/rem/lorca/issues",
      "changelog_uri"     => "https://notabug.org/rem/lorca/src/master/ChangeLog.md"
    }

    metadata = Lorca::LorcaManifest.metadata

    assert_equal expected_metadata, metadata
    assert_frozen metadata
  end

  def test_has_files
    # test/**/dev/**/*.* files are excluded because they are development helpers
    expected_files =
      %w[ChangeLog.md License.md Manifest.md ReadMe.md Security.md _expand_lib_path.rb] +
      Dir["bin/*"] +
      Dir["{lib,test}/**/*.{rb,txt,dir,pag}"] -
      Dir["test/**/dev/**/*.{rb,txt}"]

    # assert_frozen missing. Apparently Gem::Specification mutates it.
    # must be unordered because CI
    assert_equal_unordered expected_files, Lorca::LorcaManifest.files
  end

  def test_codebase_returns_required_paths_basenames
    lib = "lib"
    codebase = Lorca::LorcaManifest.codebase

    assert Lorca::LorcaManifest.files.any? { |f| %r[\A#{lib}/(.*)].match? f }
    assert_equal [lib], codebase
    assert_frozen codebase
  end

  def test__test_suite_returns_test_dir_basename
    suite = "test"
    assert_equal suite, Lorca::LorcaManifest.test_suite
    assert Lorca::LorcaManifest.files.any? { |f| %r[\A#{suite}/(.*)].match? f }
  end

  def test__tests_are_included_in_the_manifest
    tests = Lorca::LorcaManifest.tests
    files = Lorca::LorcaManifest.files

    tests.each { |t| assert_includes files, t }
    assert_frozen tests
  end

  def test_has_bindir
    assert_equal "bin", Lorca::LorcaManifest.bindir
  end

  # We don't check all binstubs in bin/ are included. Officially, there
  # can only be one
  def test_binstubs_are_among_files_in_the_manifest
    binstubs = Lorca::LorcaManifest.binstubs
    files = Lorca::LorcaManifest.files
    bindir = Lorca::LorcaManifest.bindir

    binstubs.each { |b| assert_includes files, "#{bindir}/#{b}" }

    assert_frozen binstubs
  end
end
