# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-

require_relative "./../../_config/minitest"
require "lorca/doc"

# Tests w/ assert_respond_to show we care about the method existing not what's
# returned, nor it's type. The type is implicitly check by building a gem w/o
# warnings. There's a test for that.
class TestLorcaDoc < Minitest::Test

  parallelize_me!

  def test_has_summary
    assert_respond_to Lorca::LorcaDoc, :summary
  end

  def test_has_description
    assert_respond_to Lorca::LorcaDoc, :description
  end

  def test_has_build_options
    assert_respond_to Lorca::LorcaDoc, :build_options
  end

  def test_documented_files_are_listed_in_the_manifest
    manifest = Lorca::LorcaManifest.files
    Lorca::LorcaDoc.files.each { |f| assert_includes manifest, f }
  end

  def test_appendices_are_among_files_listed_in_the_manifest
    manifest = Lorca::LorcaManifest.files
    Lorca::LorcaDoc.appendices.each { |a| assert_includes manifest, a }
  end
end
