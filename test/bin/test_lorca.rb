# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-

require_relative "./../_config/minitest"
require "rbconfig"

class TestLorcaBin < Minitest::Test
  def test_binstubs_return_usage_by_default
    skip "slow CLI Integration" unless ENV["LORCA_ALL_TESTS"]

    host = RbConfig::CONFIG["host_vendor"]
    lorca = "lorca"
    lorca += %r"alpine".match?(host) ? "-#{host}" : ""

    out, _ = capture_subprocess_io { system lorca }
    assert_match %r"usage:"i, out
  end

  def test_binstubs_permissions
    lorca = "bin/lorca"

    assert File.executable_real? lorca
    assert File.executable_real? "#{lorca}-alpine"
  end
end
