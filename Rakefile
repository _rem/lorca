# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-

require "rake/testtask"
require "rdoc/task"
require "lorca/manifest"
require "lorca/doc"

# only add tasks that are useful for anyone who `gem install lorca --development`
# tasks meant for maintainers go in `./dev/tasks/`.
FileList["dev/tasks/*.rake"].each(&method(:import)) if Dir.exist? "./dev/tasks"

task :default => :test

Rake::TestTask.new(:test) do |t|
  ENV["LORCA_ALL_TESTS"] = "1"

  t.libs.<<(*Lorca::LorcaManifest.codebase).<<(Lorca::LorcaManifest.test_suite)
  tests =  Lorca::LorcaManifest.tests
  test_dev = "test/dev"
  t.test_files = if Dir.exist? test_dev
                   tests.dup << FileList["#{test_dev}/**/test_*.rb"]
                 else
                   tests
                 end
  t.warning = true
end

RDoc::Task.new(rdoc: "doc:build", clobber_rdoc: "doc:remove", rerdoc: "doc:rebuild") do |d|
  d.generator = "hanna"
  d.rdoc_dir = "doc"
  d.rdoc_files.include(*Lorca::LorcaDoc.files).include(*Lorca::LorcaDoc.appendices)
  d.options = Lorca::LorcaDoc.build_options
end
