# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-

require "rbconfig"
require_relative "./_expand_lib_path"
require "lorca/version"
require "lorca/doc"
require "lorca/manifest"

Gem::Specification.new do |g|
  g.name          = Lorca::LorcaManifest.gem_name
  g.version       = Lorca::LorcaVersion.dup
  g.authors       = Lorca::LorcaManifest.authors
  g.email         = Lorca::LorcaManifest.contact_emails
  g.summary       = Lorca::LorcaDoc.summary
  g.description   = Lorca::LorcaDoc.description
  g.homepage      = Lorca::LorcaManifest.repo
  g.license       = Lorca::LorcaManifest.license
  g.metadata      = Lorca::LorcaManifest.metadata
  g.files         = Lorca::LorcaManifest.files
  g.require_paths = Lorca::LorcaManifest.codebase
  g.test_files    = Lorca::LorcaManifest.tests
  g.bindir        = Lorca::LorcaManifest.bindir
  g.executables.<<(*Lorca::LorcaManifest.binstubs)
  g.extra_rdoc_files = Lorca::LorcaDoc.appendices
  g.rdoc_options = Lorca::LorcaDoc.build_options

  g.required_ruby_version = ">= 2.4"

  unless RbConfig::CONFIG["ruby_version"] >= "2.5"
    g.add_runtime_dependency "sysrandom", "~> 1.0", ">= 1.0.5"
  end

  g.add_runtime_dependency "clipboard", "~> 1.3", ">= 1.3.3"

  g.add_development_dependency "bundler", "~> 2.0", ">= 2.0.1"
  g.add_development_dependency "rake", "~> 12.3", ">= 12.3.2"
  g.add_development_dependency "hanna-nouveau", "~> 1.0", ">= 1.0.3"
  g.add_development_dependency "minitest", "~> 5.11", ">= 5.11.3"
  g.add_development_dependency "minitest-unordered", "~> 1.0", ">= 1.0.2"
  g.add_development_dependency "minitest-proveit", "~> 1.0", ">= 1.0.0"
  g.add_development_dependency "minitest-autotest", "~> 1.0", ">= 1.0.3"
end
