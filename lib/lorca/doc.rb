# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-
require "lorca/manifest"

class Lorca
  # Aimed at gemspec, and supported doc generators.
  module LorcaDoc
    # One line summary of what Lorca is.
    def self.summary
      "Passphrase, and password generator"
    end

    # Short description of what Lorca does, and how does it do it.
    def self.description
      <<~EOH
      Lorca is a passphrase generator based on diceware made available by the EFF.
      Passwords, and pins are handle by the system's pseudo-random generator.
      EOH
    end

    # Array of options used for building the documentation.
    def self.build_options
      [
        "--line-numbers",
        "--title", "#{LorcaManifest.official_name}: #{self.summary}",
        "--main", "ReadMe.md"
      ]
    end

    # Array of documented files. These should be listed in Manifest.md.
    def self.files
      Lorca::LorcaManifest.files.select { |f|
        Lorca::LorcaManifest.codebase.any? { |dir| f.start_with? dir }
      }
    end

    # Array of extra doc files. These should be listed in Manifest.md.
    def self.appendices
      Lorca::LorcaManifest.files.select { |f| f.end_with? ".md" }
    end
  end
end
