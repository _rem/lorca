# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-

class Lorca
  # Major version. Updated for changes that may require users to modify their code.
  LorcaMajorVersion = 0

  # Minor version. Updated for new feature releases.
  LorcaMinorVersion = 5

  # Patch version. Updated only for bug fixes from the last feature release.
  LorcaPatchVersion = 0

  # Full version as a string.
  LorcaVersion = "#{LorcaMajorVersion}.#{LorcaMinorVersion}.#{LorcaPatchVersion}"
end
