# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-

class Lorca
  # Aimed at basic security auditing tools.
  module LorcaManifest
    # Name as used in `require` statements.
    def self.gem_name
      official_name.downcase
    end

    # Name as it's meant to be spelled.
    def self.official_name
      "Lorca"
    end

    # Array of authors' string names
    def self.authors
      ["René Maya"].freeze
    end

    # Array of maintainers' email strings.
    def self.contact_emails
      %w[rem@disroot.org].freeze
    end

    # Main repo url.
    def self.repo
      "https://notabug.org/rem/lorca"
    end

    # License type. Full text in License.md.
    def self.license
      "ISC"
    end

    # Online resources.
    def self.metadata
      {
        "source_code_uri"   => repo,
        "bug_tracker_uri"   => "https://notabug.org/rem/lorca/issues",
        "changelog_uri"     => "https://notabug.org/rem/lorca/src/master/ChangeLog.md"
      }.freeze
    end

    # Array of official gem files. Manually maintained at Manifest.md.
    #--
    # Not frozen, raises warnings building gem.
    #++
    def self.files
      File.readlines("./Manifest.md").map(&:chomp).reject(&:empty?)
    end

    # Array of directories housing the codebase.
    def self.codebase
      src = "lib"
      [src].freeze if files.any? { |f| f.start_with? "#{src}/" }
    end

    # Directory housing the test suite.
    def self.test_suite
      "test"
    end

    # Array of test files.
    def self.tests
      tests = Dir["test/**/test_*.rb"] - Dir["test/dev/**/*.rb"]
      tests.freeze
    end

    # Directory housing binstubs.
    def self.bindir
      "bin"
    end

    # Array of binstubs.
    #---
    # Officially, there can only be one
    #+++
    def self.binstubs
      %w[lorca].freeze
    end
  end
end
